/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.nomi.bird_area_analyser;

import java.io.IOException;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michiel
 */
public class BirdAreaAnalysisController {
    /**
     * starts the application.
     * @param dataSource the data source
     */
    public void start(final BirdAreaSource dataSource) {
        try {
            BirdAreaCollection collection = dataSource.getBirdAreaCollection();
            collection.printSorted();

            System.out.println("\nsorting by number of species\n");

            collection.printSorted(new Comparator<Area>(){
                @Override
                public int compare(final Area o1, final Area o2) {
                    return Integer.compare(o2.getNumberOfSpecies(), o1.getNumberOfSpecies());
                }
            });

            //or, Java8:
//            collection.printSorted(
//                    (Comparator<Area>) (Area o1, Area o2) -> Integer.compare(
//                            o2.getNumberOfSpecies(), o1.getNumberOfSpecies()));
        } catch (IOException ex) {
            Logger.getLogger(BirdAreaAnalyser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
