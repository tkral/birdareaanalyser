/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.nomi.bird_area_analyser;

import java.io.IOException;

/**
 *
 * @author michiel
 */
public interface BirdAreaSource {

    /**
     *
     * @return collection a collection of bird areas
     * @throws IOException ex
     */
    BirdAreaCollection getBirdAreaCollection()  throws IOException;
}
